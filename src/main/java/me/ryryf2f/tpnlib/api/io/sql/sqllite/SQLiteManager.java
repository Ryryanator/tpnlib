package me.ryryf2f.tpnlib.api.io.sql.sqllite;

import me.ryryf2f.tpnlib.TpnLib;

import java.util.HashMap;
import java.util.Map;

public class SQLiteManager
{

    private Map<String, SQLiteAPI> databases = new HashMap();

    public void initializeDatabase(String databaseName, String createStatement)
    {
        SQLiteDatabase db = new SQLiteDatabase(databaseName, createStatement, TpnLib.getInstance().getDataFolder());
        db.load();
        this.databases.put(databaseName, db);
    }

    public Map<String, SQLiteAPI> getDatabases()
    {
        return this.databases;
    }

    public SQLiteDatabase getDatabase(String databaseName)
    {
        return (SQLiteDatabase)getDatabases().get(databaseName);
    }

}

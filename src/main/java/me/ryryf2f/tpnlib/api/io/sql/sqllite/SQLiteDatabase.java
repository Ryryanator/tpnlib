package me.ryryf2f.tpnlib.api.io.sql.sqllite;

import me.ryryf2f.tpnlib.TpnLib;
import me.ryryf2f.tpnlib.api.util.error.TpnLibErrors;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * based off of SQLiteLib
 */
public class SQLiteDatabase extends SQLiteAPI
{
    private String dbname;
    private String createTestTable = "CREATE TABLE IF NOT EXISTS test (`test` varchar(32) NOT NULL,PRIMARY KEY (`test`));";
    private String customCreateString;
    private File dataFolder;

    /**
     * Creates a database object
     * @param databaseName
     * @param createStatement
     * @param folder
     */
    public SQLiteDatabase(String databaseName, String createStatement, File folder)
    {
        this.dbname = databaseName;
        this.customCreateString = createStatement;
        this.dataFolder = folder;
    }

    public Connection getSQLConnection()
    {
        File folder = new File(this.dataFolder, this.dbname + ".db");
        if (!folder.exists()) {
            try
            {
                folder.createNewFile();
            }
            catch (IOException e)
            {
                TpnLib.getLogHandler().severe("YamlFile write error: " + this.dbname + ".db");
            }
        }
        try
        {
            if ((this.connection != null) && (!this.connection.isClosed())) {
                return this.connection;
            }
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:" + folder);
            return this.connection;
        }
        catch (SQLException ex)
        {
            TpnLib.getLogHandler().severe("SQLite exception on initialize");
            ex.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            TpnLib.getLogHandler().severe( "You need the SQLite JBDC library. Google it. Put it in /lib folder.");
        }
        return null;
    }

    public void load()
    {
        this.connection = getSQLConnection();
        try
        {
            Statement s = this.connection.createStatement();
            s.executeUpdate(this.createTestTable);
            s.executeUpdate(this.customCreateString);
            s.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        initialize();
    }

    public File getDataFolder()
    {
        return this.dataFolder;
    }
}

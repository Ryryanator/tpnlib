package me.ryryf2f.tpnlib.api.io.sql.sqllite;


import me.ryryf2f.tpnlib.TpnLib;
import me.ryryf2f.tpnlib.api.util.error.TpnLibErrors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Database class based on SQLiteLib
 */
public abstract class SQLiteAPI
{

    protected Connection connection;
    public abstract Connection getSQLConnection();
    public abstract void load();


    public void initialize()
    {
        this.connection = getSQLConnection();
        try
        {
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM test");
            ResultSet rs = ps.executeQuery();
            close(ps, rs);
        } catch(SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_CONNECTION_ERROR);
            ex.printStackTrace();
        }
    }

    public Boolean executeStatement(String statement)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = getSQLConnection();
            return Boolean.valueOf(!ps.execute());
        }catch(SQLException ex)
        {
            Boolean localBool;
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_EXECUTE_ERROR);
            ex.printStackTrace();
            return Boolean.valueOf(false);
        }finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }catch(SQLException ex)
            {
                TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
                ex.printStackTrace();
                return Boolean.valueOf(false);
            }
        }
    }

    public Object queryValue(String statement, String row)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            conn = getSQLConnection();
            ps = conn.prepareStatement(statement);

            rs = ps.executeQuery();
            if (rs.next())
            {
                return rs.getObject(row);
            }
        }catch(SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_EXECUTE_ERROR);
            ex.printStackTrace();
        }finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }catch (SQLException ex)
            {
                TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
                ex.printStackTrace();
            }
        }
        try
        {
            if (ps != null)
            {
                ps.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }catch (SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
            ex.printStackTrace();
        }
        return null;
    }

    public List<Object> queryRow(String statement, String row)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> objects = new ArrayList();
        try
        {
            conn = getSQLConnection();
            ps = conn.prepareStatement(statement);

            rs = ps.executeQuery();
            while (rs.next()) {
                objects.add(rs.getObject(row));
            }
            return objects;
        }
        catch (SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_EXECUTE_ERROR);
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (SQLException ex)
            {
                TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
                ex.printStackTrace();
            }
        }
        return null;
    }

    public void close(PreparedStatement ps, ResultSet rs)
    {
        try
        {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        catch (SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
            ex.printStackTrace();
        }
    }

    public void closeConnection()
    {
        try
        {
            this.connection.close();
        }
        catch (SQLException ex)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.SQL_CLOSE_ERROR);
            ex.printStackTrace();
        }
    }
}
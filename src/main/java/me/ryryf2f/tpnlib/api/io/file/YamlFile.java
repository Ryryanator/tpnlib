package me.ryryf2f.tpnlib.api.io.file;

import me.ryryf2f.tpnlib.TpnLib;
import me.ryryf2f.tpnlib.api.util.error.TpnLibErrors;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Basic config class to be used Bukkit configuration
 * Created by RyryF2F
 */
public class YamlFile
{
    protected File file;
    protected FileConfiguration fileConfiguration;
    protected File directoryParent;
    protected String directoryChild;
    protected boolean hasDefault;

    /**
     * Creates flatfile using loaded parameters set in init
     * This will only create the file, it will not load the FileConfiguration.
     */
    public void createFile()
    {
        file = new File(directoryParent, directoryChild);

        if (!file.exists())
        {
            if (hasDefault)
            {
                copy();
            }else
            {
                try
                {
                    file.createNewFile();
                }catch (IOException e)
                {
                    TpnLib.getLogHandler().error(TpnLibErrors.FILE_CREATION_ERROR);
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Using parameters loaded in file, it will stream the file from the resources folder to the directory of the file
     * @return - success
     */
    public boolean copy()
    {
        boolean success = true;
        InputStream source = this.getClass().getResourceAsStream(directoryChild);

        try
        {
            Files.copy(source, Paths.get(file.getPath()));
        }catch(IOException e)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.FILE_STREAMING_ERROR);
            e.printStackTrace();
            success = false;
        }
        return success;
    }


    /**
     * Loads file configuration
     * @return - success (for null checking)
     */
    public boolean load()
    {
        if (file.exists())
        {
            fileConfiguration = YamlConfiguration.loadConfiguration(file);
            return true;
        }else
        {
            return false;
        }
    }

    /**
     * Saves file configuration to file
     */
    public void save()
    {
        try
        {
            fileConfiguration.save(file);
        }catch(IOException e)
        {
            TpnLib.getLogHandler().error(TpnLibErrors.FILE_SAVING_ERROR);
            e.printStackTrace();
        }
    }

    /**
     * flushes all data stored in the FileConfiguration
     */
    public void flush(boolean saveFile)
    {
        if (saveFile)
        {
            save();
        }
        fileConfiguration = null;
    }


}

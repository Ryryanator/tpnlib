package me.ryryf2f.tpnlib.api.util.error;


/**
 * Interface for an errors enum to allow log handler to use the errors methods
 */
public interface ErrorsInterface
{

    /**
     * Returns severity level to log at
     * @return 0 for info, 1 for warn, 2 for severe
     */
    public int getSeverity();

    /**
     * Message to log
     * @return - message
     */
    public String getMessage();

}

package me.ryryf2f.tpnlib.api.util.error;

public enum TpnLibErrors implements ErrorsInterface
{

    FILE_CREATION_ERROR(2,"Error creating file"),
    FILE_STREAMING_ERROR(2, "Error streaming file"),
    FILE_SAVING_ERROR(2, "Error saving file"),
    SQL_EXECUTE_ERROR(2, "Error executing SQL statement"),
    SQL_CLOSE_ERROR(2, "Error closing SQL database"),
    SQL_CONNECTION_ERROR(2, "Error establishing SQL connection");

    private final int severity; // 0 for info, 1 for warn, 2 for severe
    private final String message;

    TpnLibErrors(int severity, String message)
    {
        this.severity = severity;
        this.message = message;
    }

    public int getSeverity()
    {
        return this.severity;
    }

    public String getMessage()
    {
        return this.message;
    }
}

package me.ryryf2f.tpnlib.api.util;

import me.ryryf2f.tpnlib.api.util.error.ErrorsInterface;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.logging.Logger;

public class LogHandler
{


    private Plugin plugin;
    private Logger logger;

    //add new feature to override default build string to add custom prefix

    /**
     * Default constructor
     * @param plugin - Spigot Plugin
     */
    public LogHandler(Plugin plugin)
    {
        this.plugin = plugin;
        this.logger = Logger.getLogger("Minecraft");
    }

    /**
     * creates string with prefix
     * Uses plugin default information
     * @param message - string to build
     * @return - string with prefix
     */
    private String buildString(String message)
    {
        PluginDescriptionFile pdFile = plugin.getDescription();
        return "[" + pdFile.getName() + " " + pdFile.getVersion() + "] : " + message;
    }

    /**
     * creates string with prefix
     * uses given name
     * @param name - plugin id to be used
     * @param message - string to build
     * @return - string with prefix
     */
    private String buildString(String name, String message)
    {
        return "[" + name + "] : " + message;
    }


    /**
     * Sends message with priority = info.
     * @param message - string to send to console
     */
    public void info(String message)
    {
        logger.info(buildString(message));
    }

    /**
     * Sends a message with priority = info.
     * @param message - string to send to console
     * @param name - prefix
     */
    public void info(String message, String name)
    {
        logger.info(buildString(name, message));
    }

    /**
     * Sends a message with priority = warning.
     * @param message - string to send to console
     */
    public void warn(String message)
    {
        logger.warning(buildString(message));
    }

    /**
     * Sends a message with priority = warning
     * @param message - string to send to console
     * @param name - prefix
     */
    public void warn(String message, String name)
    {
        logger.warning(buildString(name, message));
    }

    /**
     * Sends a message with priority = severe.
     * @param message - string to send to console.
     */
    public void severe(String message)
    {
        logger.severe(buildString(message));
    }

    /**
     * Sends a message with priority = warning
     * @param message - string to send to console
     * @param name - prefix
     */
    public void severe(String message, String name)
    {
        logger.severe(buildString(name, message));
    }

    /**
     * Uses the ErrorsInterface enum to report error automatically
     * @param error
     */
    public void error(ErrorsInterface error)
    {
        if (error.getSeverity() == 0)
        {
            logger.info(buildString(error.getMessage()));
        }else
        {
            if (error.getSeverity() == 1)
            {
                logger.warning(buildString(error.getMessage()));
            }else
            {
                if (error.getSeverity() == 2)
                {
                    logger.severe(buildString(error.getMessage()));
                }
            }
        }
    }

    /**
     * Uses the ErrorInterface to report and error
     * @param error
     * @param message message to append to end of original error
     */
    public void error(ErrorsInterface error, String message)
    {
        if (error.getSeverity() == 0)
        {
            logger.info(buildString(error.getMessage() + " - " + message));
        }else
        {
            if (error.getSeverity() == 1)
            {
                logger.warning(buildString(error.getMessage() + " - " + message));
            }else
            {
                if (error.getSeverity() == 2)
                {
                    logger.severe(buildString(error.getMessage() + " - " + message));
                }
            }
        }
    }





}

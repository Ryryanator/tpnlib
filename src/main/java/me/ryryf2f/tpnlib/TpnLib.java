package me.ryryf2f.tpnlib;

import me.ryryf2f.tpnlib.api.util.LogHandler;
import org.bukkit.plugin.java.JavaPlugin;

public final class TpnLib extends JavaPlugin
{

    private static TpnLib instance;
    private static LogHandler logHandler;

    @Override
    public void onEnable()
    {
        instance = this;
        logHandler = new LogHandler(instance);
        logHandler.info("TpnLib Enabled :)");
    }

    @Override
    public void onDisable()
    {
        logHandler = null;
        instance = null;
    }

    public static LogHandler getLogHandler()
    {
        return logHandler;
    }

    public static TpnLib getInstance()
    {
        return instance;
    }
}
